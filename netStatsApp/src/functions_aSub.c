#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <registryFunction.h>
#include <epicsExport.h>
#include <aSubRecord.h>

#include <epicsTime.h>
#include <sys/times.h> 
#include <unistd.h> 
#include <time.h> 

//#define TICKS_PER_SEC sysconf(_SC_CLK_TCK)


static int          readFiles(aSubRecord *precord);
static long          txBytes  ();
static long          rxBytes  ();
static long          txPackets(); 
static long          rxPackets();
static int          speed    ();
static unsigned int heartBeat();


//---------------------------------------------------------------------------
static int readFiles(aSubRecord *precord) {
    static int prev_rxP = 0;  
    static int prev_txP = 0;  
    static int prev_rxB = 0;  
    static int prev_txB = 0;  

    long rxP = rxPackets();  
    long txP = txPackets();  
    long rxB = rxBytes();  
    long txB = txBytes();  

    int sp = speed();
    unsigned int hb = heartBeat();


    *(long *)precord->vala = rxB;
    *(float *)precord->valb = (rxB - prev_rxB);// / (float)sysconf(_SC_CLK_TCK);
    *(long *)precord->valc = txB;
    *(float *)precord->vald = (txB - prev_txB);// / (float)sysconf(_SC_CLK_TCK);


    *(long *)precord->valg = rxP;
    *(float *)precord->valh = (rxP - prev_rxP);// / (float)sysconf(_SC_CLK_TCK);
    *(long *)precord->vali = txP;
    *(float *)precord->valj = (txP - prev_txP);// / (float)sysconf(_SC_CLK_TCK);

    *(int *)precord->vale = sp;
    *(unsigned int *)precord->valf = hb;

   // printf("Hbeat:        %u, speed = %d\n", hb, sp);
   // printf("bytes:   rx = %ld, tx    = %ld, \n", rxB, txB);
   // printf("packets: rx = %ld, tx    = %ld, \n", rxP, txP);
    printf("SC_CLK_TICK: %ld\n",sysconf(_SC_CLK_TCK) );
    prev_rxB = rxB;
    prev_txB = txB;
    prev_rxP = rxP;
    prev_txP = txP;

return 0;
}    
//---------------------------------------------------------------------------
static unsigned int heartBeat() {

    static unsigned int heartbeat = 0;
    //printf("Hbeat = %u\n", heartbeat);

return heartbeat++;
}

//---------------------------------------------------------------------------
static long txBytes () {
    static char statfile[] = "/sys/class/net/eth0/statistics/tx_bytes";
    long bytes;
    FILE *fp;

    fp = fopen(statfile, "r");
    if (fp) {
        fscanf(fp, "%ld", &bytes);
        fclose(fp);
    }
    else bytes = -1;
    // printf("+++bytes: %ld\n", bytes);

return bytes;
}
 
//---------------------------------------------------------------------------

static long rxBytes () {
    static char statfile[] = "/sys/class/net/eth0/statistics/rx_bytes";
    long bytes;
    FILE *fp;

    fp = fopen(statfile, "r");
    if (fp) {
        fscanf(fp, "%ld", &bytes);
        fclose(fp);
    }
    else bytes = -1;
    //printf("+++bytes: %ld\n", bytes);

return bytes;
}
 
//---------------------------------------------------------------------------

static long rxPackets () {
    static char statfile[] = "/sys/class/net/eth0/statistics/rx_packets";
    long packets;
    FILE *fp;

    fp = fopen(statfile, "r");
    if (fp) {
        fscanf(fp, "%ld", &packets);
        fclose(fp);
    }
    else packets = -1;
    //printf("+++packets: %ld\n", packets);

return packets;
}
 
//---------------------------------------------------------------------------

static long txPackets () {
    static char statfile[] = "/sys/class/net/eth0/statistics/tx_packets";
    long packets;
    FILE *fp;

    fp = fopen(statfile, "r");
    if (fp) {
        fscanf(fp, "%ld", &packets);
        fclose(fp);
    }
    else packets = -1;
    //printf("+++packets: %ld\n", packets);
return packets;
}
 
//---------------------------------------------------------------------------

static int speed () {
    static char statfile[] = "/sys/class/net/eth0/speed";
    int speed;
    FILE *fp;

    fp = fopen(statfile, "r");
    if (fp) {
        fscanf(fp, "%d", &speed);
        fclose(fp);
        //printf("+++1 speed: %u\n", speed);    
    }
    else {
        speed = -1;
    };

    //printf("+++2 speed: %d\n", speed);    

return speed;
}
 
//---------------------------------------------------------------------------


epicsRegisterFunction(readFiles);

